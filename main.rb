require_relative "person.rb"
require_relative "student.rb"
require_relative "teacher.rb"

puts "-------------------------- Instanciando Person -----------------------------"

person = Person.new "12345678900", "Fabio", 24, "02/02/1995", "51, A street"
# apresenta pessoa
person.present do |cpf, name, age, birthday, address|
	puts cpf, name, age, birthday, address
end
# imprime humor
puts "\nHumor em #{:terca}: #{person.mood :terca}\n"
# imprime atividade que está fazendo
puts person.activity


puts "\n------------------------ Instanciando Student ----------------------------\n"

student = Student.new "12345678900", "Fabio", 24, "02/02/1995", "51, A street", "55550", true
# apresenta estudante
student.present do |cpf, name, age, birthday, address, reg_number, is_at_school| 
	puts cpf, name, age, birthday, address, reg_number, is_at_school
end
# movimenta estudante
student.move
puts "\nDepois de mover. Na escola: #{student.is_at_school?}\n"
# imprime humor
puts "\nHumor em #{:sexta}: #{student.mood :sexta}\n"
# imprime atividade que está fazendo
puts student.activity


puts "\n------------------------ Instanciando Teacher -------------------------------\n"

teacher = Teacher.new "12345678900", "Fabio", 24, "02/02/1995", "51, A street", ['Física', 'Pedagogia'], ['Física', 'Matemática']

teacher.present do |cpf, name, age, birthday, address, academic_bg, subjects|
	puts cpf, name, age, birthday, address, academic_bg, subjects
end
# imprime humor
puts "\nHumor em #{:quarta}: #{teacher.mood :quarta}\n"
# imprime atividade
puts teacher.activity
