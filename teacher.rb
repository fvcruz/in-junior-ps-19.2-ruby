require_relative "person.rb"

class Teacher < Person
	attr_accessor :academic_background, :subjects # formação acadêmica

	def initialize cpf="", name="", age=0, birthday="", address="", academic_bg=[], subjects=[]
		super cpf, name, age, birthday, address
		@academic_background = academic_bg 
		@subjects = subjects
	end

	def present
		yield(
			"CPF: #{@cpf}", 
			"Nome: #{@name}", 
			"Idade: #{@age}", 
			"Dt. Nasc.: #{@birthday}", 
			"Endereço: #{@address}", 
			"Formação: #{@academic_background.join(', ')}",
			"Matérias: #{@subjects.join(', ')}"
		)
	end

end
