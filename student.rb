require_relative "person.rb"

class Student < Person 

	attr_accessor :reg_number

	def initialize cpf="", name="", age=0, birthday="", address="", reg_number="", is_at_school=false
		super cpf, name, age, birthday, address
		@reg_number = reg_number
		@is_at_school = is_at_school
	end

	def is_at_school?
		return @is_at_school
	end

	def move
		@is_at_school = !@is_at_school
	end

	def present
		yield("CPF: #{@cpf}", "Nome: #{@name}", "Idade: #{@age}", "Dt. Nasc.: #{@birthday}", "Endereço: #{@address}", "Matrícula: #{@reg_number}", "Está na escola: #{@is_at_school}")
	end

	def activity
		"#{@name} está estudando"
	end
end
