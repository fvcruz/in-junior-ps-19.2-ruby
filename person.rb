class Person
	attr_accessor :cpf, :name, :age, :birthday, :address

	def initialize cpf="", name="", age=0, birthday="", address=""
		@cpf = cpf
		@name = name
		@age = age
		@birthday = birthday
		@address = address
	end

	def present
		yield("CPF: #{@cpf}", "Nome: #{@name}", "Idade: #{@age}", "Dt. Nasc.: #{@birthday}", "Endereço: #{@address}")
	end

	def mood day
		{
			segunda: "Sonolento(a)", 
			terca: "Inspirado(a)",
			quarta: "Animado(a)", 
			quinta: "Agitado(a)", 
			sexta: "Feliz", 
			sabado: "Divertido(a)",
			domingo: "Cansado(a)" 
		}[day]
	end

	def activity
		"#{@name} está trabalhando"
	end

end
